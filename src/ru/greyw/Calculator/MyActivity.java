package ru.greyw.Calculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    public void on_btclick_sloj(View view)
    {


        EditText editText;
        EditText editText2;

        editText = (EditText)findViewById(R.id.editText);
        editText2 = (EditText)findViewById(R.id.editText2);

        int a,b,res;
        a=Integer.parseInt(editText.getText().toString());//чтение editText и заполнение переменных числами
        b=Integer.parseInt(editText2.getText().toString());

        res = a + b;

        String result =a + "+" + b + "=" + res;

        Intent intent=new Intent(this, MyActivity2.class);

        intent.putExtra("result",result);

        startActivity(intent);

    }

    public void on_btclick_vych(View view)
    {


        EditText editText;
        EditText editText2;

        editText = (EditText)findViewById(R.id.editText);
        editText2 = (EditText)findViewById(R.id.editText2);

        int a,b,res;
        a=Integer.parseInt(editText.getText().toString());//чтение editText и заполнение переменных числами
        b=Integer.parseInt(editText2.getText().toString());

        res = a - b;

        String result =a + "-" + b + "=" + res;

        Intent intent=new Intent(this, MyActivity2.class);

        intent.putExtra("result",result);

        startActivity(intent);

    }

    public void on_btclick_mnoj(View view)
    {


        EditText editText;
        EditText editText2;

        editText = (EditText)findViewById(R.id.editText);
        editText2 = (EditText)findViewById(R.id.editText2);

        int a,b,res;
        a=Integer.parseInt(editText.getText().toString());//чтение editText и заполнение переменных числами
        b=Integer.parseInt(editText2.getText().toString());

        res = a * b;

        String result =a + "*" + b + "=" + res;

        Intent intent=new Intent(this, MyActivity2.class);

        intent.putExtra("result",result);

        startActivity(intent);

    }

    public void on_btclick_del(View view)
    {


        EditText editText;
        EditText editText2;

        editText = (EditText)findViewById(R.id.editText);
        editText2 = (EditText)findViewById(R.id.editText2);

        int a,b,res;
        a=Integer.parseInt(editText.getText().toString());//чтение editText и заполнение переменных числами
        b=Integer.parseInt(editText2.getText().toString());

        res = a / b;

        String result =a + "/" + b + "=" + res;

        Intent intent=new Intent(this, MyActivity2.class);

        intent.putExtra("result",result);

        startActivity(intent);

    }



}
